import React from "react";

export default function SubBrandFacet({ handleFilterClick, productSubBrands }) {
  return (
    <div class="facet-wrap facet-display">
      <strong>Product Line</strong>
      <div className="facetwp-facet">
        {Object.keys(productSubBrands).map((brand, i) => {
          if (brand && productSubBrands[brand] > 0) {
            return (
              <div>
                <span
                  id={`sub-brand-filter-${i}`}
                  key={i}
                  data-value={`${brand.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("sub_brand", e.target.dataset.value)
                  }>
                  {" "}
                  {brand} {` (${productSubBrands[brand]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
